package tn.esprit.spring.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.Equipe;

@Repository
public interface EquipeRepository extends JpaRepository<Equipe,Long> {

}
