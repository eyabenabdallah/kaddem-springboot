package tn.esprit.spring.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.DetailEquipe;

import java.util.List;

@Repository
public interface DetailEquipeRepository extends JpaRepository<DetailEquipe,Long> {
    //List<DetailEquipe> findByIdDetailEquipeIsLessThan(Long t);
}
