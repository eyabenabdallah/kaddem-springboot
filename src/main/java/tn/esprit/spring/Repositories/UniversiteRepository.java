package tn.esprit.spring.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.Universite;

@Repository
public interface UniversiteRepository extends JpaRepository<Universite,Long> {

}
