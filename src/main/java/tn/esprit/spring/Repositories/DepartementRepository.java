package tn.esprit.spring.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Etudiant;

import java.util.List;

@Repository
public interface DepartementRepository extends JpaRepository<Departement,Long > {
   // List<Departement> findByNomDepartIsLikeAnd(String n);
    // List<Departement>  findDepartementByEtudiantsExists(Etudiant e);
}
