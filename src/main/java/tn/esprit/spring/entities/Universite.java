package tn.esprit.spring.entities;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Table( name ="Universite")
public class Universite implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idUniv")

    private Long idUniv; // Clé primaire
    private String nomUniv;

    @OneToMany (cascade = CascadeType.ALL)
    private Set<Departement> departements;
}
