package tn.esprit.spring.entities;

public enum Niveau {
    JUNIOR,
    SENIOR,
    EXPERT
}
