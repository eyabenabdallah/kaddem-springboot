package tn.esprit.spring.entities;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Table( name ="DetailEquipe")
public class DetailEquipe implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idDetailEquipe")

    private long idDetailEquipe;

    private int salle;

    private String thematique;

    @OneToOne
    private Equipe equipe;

}
