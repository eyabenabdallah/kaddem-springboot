package tn.esprit.spring.entities;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Slf4j
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@ToString

public class Departement implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idDepart")

    private Long idDepart; // Clé primaire
    private String nomDepart;

    @OneToMany(mappedBy = "departement")
    private Set<Etudiant> etudiants;
}
