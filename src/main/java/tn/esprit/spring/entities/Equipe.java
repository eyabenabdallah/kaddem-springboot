package tn.esprit.spring.entities;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Table( name ="Equipe")
public class Equipe implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idEquipe")

    private Long idEquipe; // Clé primaire
    private String nomEquipe;

    @Enumerated(EnumType.STRING)
    private Niveau niv;

    @OneToOne (mappedBy = "equipe")
    private DetailEquipe detailEquipe;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Etudiant> etudiants;
}
