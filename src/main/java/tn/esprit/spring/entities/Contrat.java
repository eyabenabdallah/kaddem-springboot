package tn.esprit.spring.entities;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Contrat implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idContart")

    private Long idContrat; // Clé primaire
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="dd/MM/yy")
    private Date dateDebutContart;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="dd/MM/yy")
    private Date dateFinContart;

    @Enumerated(EnumType.STRING)
    private Specialite spec;

    @ManyToOne
    private Etudiant etudiant;

    private boolean archive;
    private int montantContrat;


}
