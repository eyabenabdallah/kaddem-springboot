package tn.esprit.spring.entities;
import lombok.*;
import lombok.experimental.FieldDefaults;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table( name ="Etudiant")
public class Etudiant implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idEtudiant")
    private Long idEtudiant; // Clé primaire
    private String prenomE;
    private String nomE;

    @Enumerated(EnumType.STRING)
    private Option opt;

    @OneToMany(mappedBy = "etudiant",cascade = CascadeType.ALL)
    private Set<Contrat> contrats;

    @ManyToMany(mappedBy = "etudiants",cascade = CascadeType.ALL)
    private Set<Equipe> equipes;

    @ManyToOne (cascade = CascadeType.ALL)
    private Departement departement;
}