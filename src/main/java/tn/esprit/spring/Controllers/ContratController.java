package tn.esprit.spring.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.ContratService;
import tn.esprit.spring.entities.Contrat;

import java.util.List;

@RestController
@RequestMapping("/Contrat")
public class ContratController {
    @Autowired
    ContratService contratService ;
    @PostMapping("/addContrat")
    public Contrat addContrat (@RequestBody Contrat c){
        return contratService.addContrat(c);
    }
    @GetMapping(value = "/getAllContrat")
    @ResponseBody
    public List<Contrat> getAllContrat() {
        return  contratService.getAllContrats();
    }


    @PutMapping("/updateContrat")
    @ResponseBody
    public void updateContrat(@RequestBody Contrat e){
        contratService.update(e);
    }


    @DeleteMapping("/deleteContrat")
    @ResponseBody
    public void deleteStudent (@RequestBody Contrat e){
        contratService.delete(e);
    }




}
