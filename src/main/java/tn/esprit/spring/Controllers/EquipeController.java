package tn.esprit.spring.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.DetailEquipeService;
import tn.esprit.spring.Services.EquipeService;
import tn.esprit.spring.entities.DetailEquipe;
import tn.esprit.spring.entities.Equipe;

import java.util.List;

@RestController
@RequestMapping("/Equipe")
public class EquipeController {
    @Autowired
    EquipeService equipeService ;
    @PostMapping("/ajouterEquipe")
    public Equipe addDEquipe (@RequestBody Equipe e){
        return equipeService.addEquipe(e);
    }
    @GetMapping(value = "/getAllEquipe")
    @ResponseBody
    public List<Equipe> getAllEquipes() {
        return  equipeService.getAllEquipes();
    }


    @PutMapping("/updateEquipe")
    @ResponseBody
    public void updateEquipe(@RequestBody Equipe e){
        equipeService.update(e);
    }


    @DeleteMapping("/deleteEquipe")
    @ResponseBody
    public void deleteEquipe(@RequestBody Equipe e){
        equipeService.delete(e);
    }

}
