package tn.esprit.spring.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.DepartementService;
import tn.esprit.spring.Services.DetailEquipeService;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.DetailEquipe;

import java.util.List;

@RestController
@RequestMapping("/DetailEquipe")
public class DetailEquipeController {
    @Autowired
    DetailEquipeService detailEquipeService ;
    @PostMapping("/ajouterDetailEquipe")
    public DetailEquipe addDetailEquipe (@RequestBody DetailEquipe d){
        return detailEquipeService.addDetailEquipe(d);
    }
    @GetMapping(value = "/getAllDetailEquipe")
    @ResponseBody
    public List<DetailEquipe> getAllDetailEquipes() {
        return  detailEquipeService.getAllDetailEquipes();
    }


    @PutMapping("/updateDetailEquipe")
    @ResponseBody
    public void updateDetailequipe(@RequestBody DetailEquipe d){
        detailEquipeService.update(d);
    }


    @DeleteMapping("/deleteDetailEquipe")
    @ResponseBody
    public void deleteDetailEquipe(@RequestBody DetailEquipe d){
        detailEquipeService.delete(d);
    }

}
