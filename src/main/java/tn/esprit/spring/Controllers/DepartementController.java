package tn.esprit.spring.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.ContratService;
import tn.esprit.spring.Services.DepartementService;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;

import java.util.List;

@RestController
@RequestMapping("/Departement")
public class DepartementController {
    @Autowired
    DepartementService departementService ;
    @PostMapping("/ajouterDepartement")
    public Departement addDepartement (@RequestBody Departement d){
        return departementService.addDepartement(d);
    }
    @GetMapping(value = "/getAllDepartements")
    @ResponseBody
    public List<Departement> getAllDepartements() {
        return  departementService.getAllDepartements();
    }


    @PutMapping("/updateDepartement")
    @ResponseBody
    public void updateDepartement(@RequestBody Departement d){
        departementService.update(d);
    }


    @DeleteMapping("/deleteDepartement")
    @ResponseBody
    public void deleteDepartement (@RequestBody Departement d){
        departementService.delete(d);
    }


}
