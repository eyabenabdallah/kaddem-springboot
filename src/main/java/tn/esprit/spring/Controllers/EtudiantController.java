package tn.esprit.spring.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.EquipeService;
import tn.esprit.spring.Services.EtudiantService;
import tn.esprit.spring.entities.Equipe;
import tn.esprit.spring.entities.Etudiant;

import java.util.List;

@RestController
@RequestMapping("/Etudiant")
public class EtudiantController {
    @Autowired
    EtudiantService etudiantService ;
    @PostMapping("/ajouterEtudiant")
    public Etudiant addDEtudiant (@RequestBody Etudiant e){
        return etudiantService.addEtudiant(e);
    }
    @GetMapping(value = "/getAllEtudiant")
    @ResponseBody
    public List<Etudiant> getAllEtudiants() {
        return  etudiantService.getAllEtudiants();
    }


    @PutMapping("/updateEtudiant")
    @ResponseBody
    public void updateEtudiant(@RequestBody Etudiant e){
        etudiantService.update(e);
    }


    @DeleteMapping("/deleteEtudiant")
    @ResponseBody
    public void deleteEtudiant(@RequestBody Etudiant e){
        etudiantService.delete(e);
    }

}
