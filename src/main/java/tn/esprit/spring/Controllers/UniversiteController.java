package tn.esprit.spring.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.Services.EtudiantService;
import tn.esprit.spring.Services.UniversiteService;
import tn.esprit.spring.entities.Etudiant;
import tn.esprit.spring.entities.Universite;

import java.util.List;

@RestController
@RequestMapping("/Universite")
public class UniversiteController {
    @Autowired
    UniversiteService universiteService ;
    @PostMapping("/addUniversite")
    public Universite addUniversite (@RequestBody Universite u){
        return universiteService.addUniversite(u);
    }
    @GetMapping(value = "/getAllUniversite")
    @ResponseBody
    public List<Universite> getAllUniversites() {
        return  universiteService.getAllUniversites();
    }


    @PutMapping("/updateUniversite")
    @ResponseBody
    public void updateUniversite(@RequestBody Universite u){
        universiteService.update(u);
    }


    @DeleteMapping("/deleteUniversite")
    @ResponseBody
    public void deleteUniversite(@RequestBody Universite u){
        universiteService.delete(u);
    }

}
