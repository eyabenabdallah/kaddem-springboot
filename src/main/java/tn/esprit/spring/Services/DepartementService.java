package tn.esprit.spring.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.Repositories.DepartementRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;

import java.util.List;

@Service
public class DepartementService {
    @Autowired
    private DepartementRepository departementRepository;

    public Departement addDepartement(Departement departement){
        return departementRepository.save(departement);
    }
    public List<Departement> getAllDepartements() {
        return departementRepository.findAll();
    }

    public void update(Departement departement) {
        departementRepository.save(departement);
    }
    public void delete(Departement departement) {
        departementRepository.delete(departement);
    }
}
