package tn.esprit.spring.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;

import java.util.List;

@Service
public class ContratService {
    @Autowired
    private ContratRepository contratRepository;

    public Contrat addContrat(Contrat contrat){
        return contratRepository.save(contrat);
    }
    public List<Contrat> getAllContrats() {
        return contratRepository.findAll();
    }

    public void update(Contrat c) {
        contratRepository.save(c);
    }
    public void delete(Contrat c) {
        contratRepository.delete(c);
    }
}
