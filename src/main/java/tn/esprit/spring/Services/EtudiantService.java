package tn.esprit.spring.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.Repositories.EtudiantRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Etudiant;

import java.util.List;

@Service
public class EtudiantService {
    @Autowired
    private EtudiantRepository etudiantRepository;

    public Etudiant addEtudiant(Etudiant etudiant){
        return etudiantRepository.save(etudiant);
    }
    public List<Etudiant> getAllEtudiants() {
        return etudiantRepository.findAll();
    }

    public void update(Etudiant etudiant) {
        etudiantRepository.save(etudiant);
    }
    public void delete(Etudiant etudiant) {
        etudiantRepository.delete(etudiant);
    }
}
