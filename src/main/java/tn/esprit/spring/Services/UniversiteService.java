package tn.esprit.spring.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.Repositories.UniversiteRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Etudiant;
import tn.esprit.spring.entities.Universite;

import java.util.List;

@Service

public class UniversiteService {
    @Autowired
    private UniversiteRepository universiteRepository;

    public Universite addUniversite(Universite universite){
        return universiteRepository.save(universite);
    }
    public List<Universite> getAllUniversites() {
        return universiteRepository.findAll();
    }

    public void update(Universite universite) {
        universiteRepository.save(universite);
    }
    public void delete(Universite universite) {
        universiteRepository.delete(universite);
    }
}
