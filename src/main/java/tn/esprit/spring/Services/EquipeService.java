package tn.esprit.spring.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.Repositories.EquipeRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Equipe;

import java.util.List;

@Service

public class EquipeService {
    @Autowired
    private EquipeRepository equipeRepository;

    public Equipe addEquipe(Equipe equipe){
        return equipeRepository.save(equipe);
    }
    public List<Equipe> getAllEquipes() {
        return equipeRepository.findAll();
    }

    public void update(Equipe equipe) {
        equipeRepository.save(equipe);
    }
    public void delete(Equipe equipe) {
        equipeRepository.delete(equipe);
    }
}
