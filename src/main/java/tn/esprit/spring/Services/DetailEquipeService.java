package tn.esprit.spring.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.Repositories.ContratRepository;
import tn.esprit.spring.Repositories.DetailEquipeRepository;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.DetailEquipe;

import java.util.List;

@Service
public class DetailEquipeService {
    @Autowired
    private DetailEquipeRepository detailEquipeRepository;

    public DetailEquipe addDetailEquipe(DetailEquipe d){
        return detailEquipeRepository.save(d);
    }
    public List<DetailEquipe> getAllDetailEquipes() {
        return detailEquipeRepository.findAll();
    }

    public void update(DetailEquipe d) {
        detailEquipeRepository.save(d);
    }
    public void delete(DetailEquipe d) {
        detailEquipeRepository.delete(d);
    }
}
